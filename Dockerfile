FROM ubuntu:22.04

RUN apt update && apt install -y \
    cmake\
    build-essential \
    cppcheck \
    lcov \
    clang-tidy \
    clang-format \    
    python3-pip && \
    
    #Clean up
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*   &&\

    #Add conan
    pip3 install --no-cache-dir conan && \
    conan profile detect